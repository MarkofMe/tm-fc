﻿namespace Finance.Controllers

open System
open System.Web.Http
open Finance.BalanceCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("Balance")>]
type balanceController() =
    inherit ApiController()

    [<Route("Balance")>]
    member this.Balance (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetBalance(jsonContent)

    [<Route("AllBalance")>]
    member this.AllBalance (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("BalanceByID")>]
    member this.BalanceByID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)