﻿namespace Finance.Controllers

open System
open System.Web.Http
open Finance.ExpenseCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("Expense")>]
type expenseController() =
    inherit ApiController()

    [<Route("Expenses")>]
    member this.Expenses (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByStaffID")>]
    member this.ByStaffID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllByStaffID(jsonContent)

    [<Route("ExpenseByID")>]
    member this.ExpenseByID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)

    [<Route("InsertExpense")>]
    member this.InsertExpense (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertExpense(jsonContent)

    [<Route("ApproveExpense")>]
    member this.ApproveExpense (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ApproveExpense(jsonContent)

    [<Route("RejectExpense")>]
    member this.RejectExpense (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        RejectExpense(jsonContent)