﻿namespace Finance.Controllers

open System
open System.Web.Http
open Finance.InvoiceCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("Invoice")>]
type invoiceController() =
    inherit ApiController()

    [<Route("Invoices")>]
    member this.Invoices (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByDealership")>]
    member this.ByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllByDealership(jsonContent)

    [<Route("InvoiceByID")>]
    member this.InvoiceByID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)
