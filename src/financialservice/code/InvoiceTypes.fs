﻿namespace Finance

open System

module InvoiceTypes =
    
    type InvoiceAuth = {
                        UserName : string
                        Password : string
                        }

    type InvoiceAuthDealership = {
                        UserName : string
                        Password : string
                        DealershipName : string
                        }

    type InvoiceAuthplusID = {
                            UserName : string
                            Password : string
                            InvoiceID : Nullable<int>
                            }

    type position = {
                    Position : string
                    }

    type Invoice = {
                    InvoiceID : Nullable<int>
                    CustomerName : string
                    Amount : Nullable<float>
                    Date : Nullable<DateTime>
                    SalesID : Nullable<int>
                    ServiceID : Nullable<int>
                    TradeID : Nullable<int>
                    }
                    