﻿namespace Finance

open System
open System.Net
open Newtonsoft.Json
open ExpenseTypes
open DataLayer.DLCode

module ExpenseCode =

    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptyExpense = {
                        ExpenseID = nullable 0
                        ExpenseType = ""
                        Amount = nullable 0.0
                        StaffName = ""
                        Date = nullable(new DateTime())
                        Notes = ""
                        Status = ""
                        }

    let AuthCheck (creds : ExpenseAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
    
    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<ExpenseAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                        query {
                            for row in schema.ExpenseGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ExpenseID = row.ExpenseID
                                                    ExpenseType = row.ExpenseType
                                                    Amount = row.Amount
                                                    StaffName = row.StaffName
                                                    Date = row.Date
                                                    Notes = row.Notes
                                                    Status = row.ExpensesStatus
                                                    }) |> Seq.toArray
            |_-> [|emptyExpense|]
        with ex ->
            [|emptyExpense|]

    let GetAllByStaffID(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<ExpenseAuthStaffID>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                                StaffID = nullable 0
                            }

            let authcheck = AuthCheck ({
                                            UserName = creds.UserName
                                            Password = creds.Password
                                        })

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                        query {
                            for row in schema.ExpenseGetByStaffID(creds.StaffID) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ExpenseID = row.ExpenseID
                                                    ExpenseType = row.ExpenseType
                                                    Amount = row.Amount
                                                    StaffName = row.StaffName
                                                    Date = row.Date
                                                    Notes = row.Notes
                                                    Status = row.ExpensesStatus
                                                    }) |> Seq.toArray
            |_-> [|emptyExpense|]
        with ex ->
            [|emptyExpense|]
            
    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ExpenseAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ExpenseID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    query {
                        for row in schema.ExpenseGetByID(id.ExpenseID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                    ExpenseID = row.ExpenseID
                                                    ExpenseType = row.ExpenseType
                                                    Amount = row.Amount
                                                    StaffName = row.StaffName
                                                    Date = row.Date
                                                    Notes = row.Notes
                                                    Status = row.ExpensesStatus
                                                    }) |> Seq.toArray
            |_-> [|emptyExpense|]
        with ex ->
            [|emptyExpense|]
    
    let insertExpense(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertExpense>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ExpenseType = ""
                            Amount = nullable 0.0
                            StaffID = nullable 0
                            Notes = ""
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    schema.ExpenseInsert(id.StaffID, id.ExpenseType, id.Amount, id.Notes) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized
        with ex ->
            HttpStatusCode.InternalServerError

    let ApproveExpense (data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<ExpenseAuthplusID>(data) //typecast data
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ExpenseID = nullable 0
                        }

            let creds = //Extract user username and password
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds //Get user position

            match authcheck.Position.Equals("Admin") with
            | true -> //If admin
                    schema.ExpenseApprove(id.ExpenseID) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized //If not admin
        with ex ->
            HttpStatusCode.InternalServerError //Triggered if the main code fails

    let RejectExpense (data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<ExpenseAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ExpenseID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                    schema.ExpenseReject(id.ExpenseID) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized
        with ex ->
            HttpStatusCode.InternalServerError
