﻿namespace Finance

open System
open System.Net
open Newtonsoft.Json
open InvoiceTypes
open DataLayer.DLCode

module InvoiceCode =

    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptyInvoice = {
                        InvoiceID = nullable 0
                        CustomerName = ""
                        Amount = nullable 0.0
                        Date = nullable(new DateTime())
                        SalesID = nullable 0
                        ServiceID = nullable 0
                        TradeID = nullable 0
                        }

    let AuthCheck (creds : InvoiceAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]

    
    let GetAll(data : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<InvoiceAuth>(data)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                        query {
                            for row in schema.InvoiceGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    InvoiceID = row.InvoiceID
                                                    CustomerName = row.CustomerName
                                                    Amount = row.Amount
                                                    Date = row.Date
                                                    SalesID = row.SaleID
                                                    ServiceID = row.ServiceID
                                                    TradeID = row.TradeID
                                                    }) |> Seq.toArray
            |_-> [|emptyInvoice|]
        with ex ->
            [|emptyInvoice|]
            
    let GetAllByDealership(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<InvoiceAuthDealership>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                                DealershipName = ""
                            }

            let authcheck = AuthCheck ({
                                            UserName = creds.UserName
                                            Password = creds.Password
                                        })

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                        query {
                            for row in schema.InvoiceGetByDealership(creds.DealershipName) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    InvoiceID = row.InvoiceID
                                                    CustomerName = row.CustomerName
                                                    Amount = row.Amount
                                                    Date = row.Date
                                                    SalesID = row.SaleID
                                                    ServiceID = row.ServiceID
                                                    TradeID = row.TradeID
                                                    }) |> Seq.toArray
            |_-> [|emptyInvoice|]
        with ex ->
            [|emptyInvoice|]
            
    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<InvoiceAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            InvoiceID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    query {
                        for row in schema.InvoiceGetByID(id.InvoiceID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                InvoiceID = row.InvoiceID
                                                CustomerName = row.CustomerName
                                                Amount = row.Amount
                                                Date = row.Date
                                                SalesID = row.SaleID
                                                ServiceID = row.ServiceID
                                                TradeID = row.TradeID
                                                }) |> Seq.toArray
            |_-> [|emptyInvoice|]
        with ex ->
            [|emptyInvoice|]