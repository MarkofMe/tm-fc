﻿namespace Finance

open System

module BalanceTypes =

    type BalanceAuth = {
                        UserName : string
                        Password : string
                        }

    type BalanceAuthplusID = {
                            UserName : string
                            Password : string
                            BalanceID : Nullable<int>
                            }

    type position = {
                    Position : string
                    }

    type Balance = {
                    BalanceID : Nullable<int>
                    NewBalance : Nullable<float>
                    Amount : Nullable<float>
                    EventType : string
                    StaffName : string
                    Name : string
                    SalesID : Nullable<int>
                    TradeID : Nullable<int>
                    ServiceID : Nullable<int>
                    InvoiceID : Nullable<int>
                    ExpenseID : Nullable<int>
                    Date : Nullable<DateTime>
                    }

    type BalanceByID = {
                    BalanceID : Nullable<int>
                    NewBalance : Nullable<float>
                    Amount : Nullable<float>
                    OldBalance : Nullable<float>
                    EventType : string
                    StaffName : string
                    Name : string
                    SalesID : Nullable<int>
                    TradeID : Nullable<int>
                    ServiceID : Nullable<int>
                    InvoiceID : Nullable<int>
                    ExpenseID : Nullable<int>
                    Date : Nullable<DateTime>
                    }
    
    type InsertBalance = {
                            Ammount: float
                            EventType: string
                            StaffID: int
                            DealershipID: int
                            SalesID: int
                            TradeID: int
                            ServiceID: int
                            InvoiceID: int
                            ExpenseID: int
                            }