﻿namespace Finance

open System
open System.Net
open Newtonsoft.Json
open BalanceTypes
open DataLayer.DLCode

module BalanceCode =

    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptyBalance = {
                        BalanceID = nullable 0
                        NewBalance = nullable 0.0
                        Amount = nullable 0.0
                        EventType = ""
                        StaffName = ""
                        Name = ""
                        SalesID = nullable 0
                        TradeID = nullable 0
                        ServiceID = nullable 0
                        InvoiceID = nullable 0
                        ExpenseID = nullable 0
                        Date = nullable(new DateTime())
                        }
    
    let emptyBalanceByID = {
                        BalanceID = nullable 0
                        NewBalance = nullable 0.0
                        Amount = nullable 0.0
                        OldBalance = nullable 0.0
                        EventType = ""
                        StaffName = ""
                        Name = ""
                        SalesID = nullable 0
                        TradeID = nullable 0
                        ServiceID = nullable 0
                        InvoiceID = nullable 0
                        ExpenseID = nullable 0
                        Date = nullable(new DateTime())
                        }

    let AuthCheck (creds : BalanceAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
        

    let GetBalance(auth : string) =
        try

            let creds = try
                            JsonConvert.DeserializeObject<BalanceAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds
            
            match authcheck.Position.Equals("Admin") with
            | true ->
                    query {
                            for row in schema.BalanceGet() do
                                    select row
                                } |> Seq.map (fun row -> row.NewBalance.Value)|> Seq.toArray
            |_-> [|0.0|]
        with ex ->
            [|0.0|]
    
    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<BalanceAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                        query {
                            for row in schema.BalanceGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    BalanceID = row.BalanceID
                                                    NewBalance = row.NewBalance
                                                    Amount = row.Amount
                                                    EventType = row.EventType
                                                    StaffName = row.StaffName
                                                    Name = row.Name
                                                    SalesID = row.SalesID
                                                    TradeID = row.TradeID
                                                    ServiceID = row.ServiceID
                                                    InvoiceID = row.InvoiceID
                                                    ExpenseID = row.ExpenseID
                                                    Date = row.Date
                                                    }) |> Seq.toArray
            |_-> [|emptyBalance|]
        with ex ->
            [|emptyBalance|]
            
    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<BalanceAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            BalanceID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                    query {
                        for row in schema.BalanceGetByID(id.BalanceID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                BalanceID = row.BalanceID
                                                NewBalance = row.NewBalance
                                                Amount = row.Amount
                                                OldBalance = row.OldBalance
                                                EventType = row.EventType
                                                StaffName = row.StaffName
                                                Name = row.Name
                                                SalesID = row.SalesID
                                                TradeID = row.TradeID
                                                ServiceID = row.ServiceID
                                                InvoiceID = row.InvoiceID
                                                ExpenseID = row.ExpenseID
                                                Date = row.Date
                                                }) |> Seq.toArray
            |_-> [|emptyBalanceByID|]
        with ex ->
            [|emptyBalanceByID|]
    