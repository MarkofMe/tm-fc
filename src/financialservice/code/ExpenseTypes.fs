﻿namespace Finance

open System

module ExpenseTypes =
    
    type ExpenseAuth = {
                        UserName : string
                        Password : string
                        }

    type ExpenseAuthStaffID = {
                        UserName : string
                        Password : string
                        StaffID : Nullable<int>
                        }

    type ExpenseAuthplusID = {
                            UserName : string
                            Password : string
                            ExpenseID : Nullable<int>
                            }

    type position = {
                    Position : string
                    }
    
    type Expense = {
                    ExpenseID : Nullable<int>
                    ExpenseType : string
                    Amount : Nullable<float>
                    StaffName : string
                    Date : Nullable<DateTime>
                    Notes : string
                    Status : string
                    }
    
    type InsertExpense = {
                            UserName : string
                            Password : string
                            ExpenseType : string
                            Amount : Nullable<float>
                            StaffID : Nullable<int>
                            Notes : string
                            }
                            